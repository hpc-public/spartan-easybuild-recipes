WebKitGTK+-2.24.1-foss-2019b built by Dan Tosello on 2020-04-02
NOTES:
	- The eb patch is necessary, but in future we might look at libsecret or libnotify etc etc
	- Additionally, the patch i wrote takes care of some missing semicolons
	- I ALSO needed to patch some missing semicolons in Source/WTF/wtf/URLHelpers.cpp
		- lines 753 & 765. Note this may be masked by Assertion.cpp macros failing.
	- Finally, i had to manually add '-ljpeg' to the link command for libwebkit2gtk-4.0.so.37 during the build
