name = 'VEP'
version = '101.0'
versionsuffix = '-Perl-%(perlver)s'

homepage = 'https://www.ensembl.org/info/docs/tools/vep'
description = """Variant Effect Predictor (VEP) determines the effect of your
 variants (SNPs, insertions, deletions, CNVs or structural variants) on genes,
 transcripts, and protein sequence, as well as regulatory regions.
 Includes EnsEMBL-XS, which provides pre-compiled replacements for frequently
 used routines in VEP."""

toolchain = {'name': 'foss', 'version': '2019b'}

source_urls = ['https://github.com/Ensembl/ensembl-vep/archive/release/']
sources = ['%(version)s.tar.gz']
checksums = ['0c640faa0a392b8789eff6891bc6922bc3ab91633273af6e15fe9337f7448c69']

dependencies = [
    ('Perl', '5.30.0'),
    ('DBD-mysql', '4.050', versionsuffix),
    ('BioPerl', '1.6.924', versionsuffix),
    ('Bio-DB-HTS', '3.01', versionsuffix),
]

exts_defaultclass = 'PerlModule'
exts_filter = ("perldoc -lm %(ext_name)s ", "")

exts_list = [
    ('Bio::EnsEMBL::XS', '2.3.2', {
        'source_tmpl': '%(version)s.tar.gz',
        'source_urls': ['https://github.com/Ensembl/ensembl-xs/archive'],
        'checksums': ['aafc59568cd1042259196575e99cdfeef9c0fb7966e5f915cfaf38c70885ffa5'],
    }),
]

moduleclass = 'bio'
